TYPE
  ConstFlattened = flat(snum)
  BoolFlattened = flat(bool)
  Interval = ConstFlattened*ConstFlattened
  State = Var -> Interval


PROBLEM Interval_Propagation
  direction  : forward
  carrier    : State
  init       : bot
  init_start : [-> (top,top)]
  combine    : join


TRANSFER
  // in assignments calculate the new value of the variable and add it to the state
  ASSIGN(variable, expression) = if (@ = bot) then @ else
      @\[variable -> evalAExp(expression, @)] endif

  // in procedur calls pass the value of the actual argument to the formal parameter
  CALL(_, param, exp), call_edge =
      @\[param -> evalAExp(exp, @)]

  CALL(_, _, _), local_edge = bot

  // at the end of procedures reset the formal parameter
  END(_, param) =
      @\[param -> top]

  // test conditions
  IF(expression), true_edge = branch(expression,@,true)

  IF(expression), false_edge = branch(expression,@,false)

  // loop conditions
  WHILE(expression), true_edge = branch(expression,@,true)

  WHILE(expression), false_edge = branch(expression,@,false)


SUPPORT
 // BEHAVIOUR

   join :: State * State -> State
   join(a, b) = crunch(a,b,join_itv)

   join_itv :: Interval * Interval -> Interval
   join_itv(i1,i2) = case (i1,i2) of
      ((bot,bot),_) => i2;
      (_,(bot,bot)) => i1;
      ((top,top),(top,top)) => (top,top);
      ((top,top),_) => i2;
      (_,(top,top)) => i1;
      ((top,bot),_) => i2;
      (_,(top,bot)) => i1;
      (_,_) => unionItv(i1,i2);
      endcase

   branch :: Expression * State * bool -> State
   branch(expression, state, edge) = 
      let valexp = evalBExp(expression,state) in
      case valexp of
        top => updateState(expression, state, edge);
        bot => bot;
        _   => if drop(valexp)=edge then
                  updateState(expression, state, edge)
               else bot endif;
        endcase

 // EXPRESSIONS

   evalAExp :: Expression * State -> Interval
   evalAExp(expression, state) =
     case expType(expression) of
       "ARITH_BINARY" => 
         let i1 = evalAExp(expSubLeft(expression),  state),
             i2 = evalAExp(expSubRight(expression), state) in
         case expOp(expression) of
           "+" => addItv(i1, i2);
           "-" => subItv(i1, i2);
           "*" => multItv(i1, i2);
           "/" => divItv(i1, i2);
           endcase;
       "ARITH_UNARY" => case expOp(expression) of
           "-" => let val = evalAExp(expSub(expression), state) in
                  negItv(val);
           endcase;
       "VAR"   => state ( expVar(expression) );
       "CONST" => (lift(expVal(expression)),lift(expVal(expression)));
       _       => error("Runtime Error: evalAExp applied to nonarithmetic Expression");
       endcase

   evalBExp :: Expression * State -> BoolFlattened
   evalBExp(expression,state) =
     case expType(expression) of
       "TRUE" => lift(true);
       "FALSE" => lift(false);
       "BOOL_UNARY" => let valexp <= evalBExp(expSub(expression), state) in lift(!valexp);
       "BOOL_BINARY" => 
         let i1 = evalAExp(expSubLeft(expression),  state),
             i2 = evalAExp(expSubRight(expression), state) in
         case expOp(expression) of
           "<"  => infItvB(i1,i2);
           "<=" => infEqItvB(i1,i2);
           ">"  => supItvB(i1,i2);
           ">=" => supEqItvB(i1,i2);
           "="  => eqItvB(i1,i2);
           "<>" => diffItvB(i1,i2);
           endcase;
       _ => top;
       endcase

   updateState :: Expression * State * bool -> State
   updateState(expression, state, edge) =
     case expType(expression) of
       "TRUE" => state;
       "FALSE" => state;
       "BOOL_UNARY" => state;
       "BOOL_BINARY" => 
         let leftExp = expSubLeft(expression),
             itv = getBItv(expression, state, edge) in
         constraintSolve(leftExp,state,itv);
       _ => state;
       endcase

   constraintSolve :: Expression * State * Interval -> State 
   constraintSolve(expression,state,itv) = 
     case expType(expression) of
       "ARITH_BINARY" => state; // constraint solving not implemented
       "ARITH_UNARY" => 
          updateVar(expVar(expression),negItv(itv),state);
       "VAR" => 
          updateVar(expVar(expression),itv,state);
       "CONST" => state;
       _ => error("Runtime Error: non arithmetic expression");
       endcase
   
   updateVar :: Var * Interval * State -> State
   updateVar(var,val,state) =
      state\[var -> val]

   getBItv :: Expression * State * bool -> Interval
   getBItv(expression,state,edge) =
     case expType(expression) of
       "TRUE" => (bot,top);
       "FALSE" => (top,bot);
       "BOOL_UNARY" => if expType(expression) = "TRUE" 
                       then (top,bot) 
                       else (bot,top) endif;
       "BOOL_BINARY" => 
         let i1 = evalAExp(expSubLeft(expression),  state),
             i2 = evalAExp(expSubRight(expression), state) in
         case expOp(expression) of
           "<"  => if edge then infItv(i1,i2)
                           else supEqItv(i1,i2) endif;
           "<=" => if edge then infEqItv(i1,i2)
                           else supItv(i1,i2) endif;
           ">"  => if edge then supItv(i1,i2)
                           else infEqItv(i1,i2) endif;
           ">=" => if edge then supEqItv(i1,i2)
                           else infItv(i1,i2) endif;
           "="  => if edge then eqItv(i1,i2)
                           else diffItv(i1,i2) endif;
           "<>" => if edge then diffItv(i1,i2)
                           else eqItv(i1,i2) endif;
           endcase;
       _ => top;
       endcase

 // CONST FLATTENED COMPARISON

   lt :: ConstFlattened * ConstFlattened -> bool
   lt(c1,c2) = case (c1,c2) of
      (top,top) => false;
      (top,top) => false;
      (bot,_) => true;
      (_,bot) => false;
      (top,_) => false;
      (_,top) => true;
      (_,_) => drop(c1) < drop(c2);
      endcase

   gt :: ConstFlattened * ConstFlattened -> bool
   gt(c1,c2) = lt(c2,c1)

   eq :: ConstFlattened * ConstFlattened -> bool
   eq(c1,c2) = !lt(c1,c2) && !gt(c1,c2)

   ltEq :: ConstFlattened * ConstFlattened -> bool
   ltEq(c1,c2) = lt(c1,c2) || eq(c1,c2)

   gtEq :: ConstFlattened * ConstFlattened -> bool
   gtEq(c1,c2) = ltEq(c2,c1)

   min :: ConstFlattened * ConstFlattened -> ConstFlattened
   min(c1, c2) = if lt(c1,c2) then c1 else c2 endif

   max :: ConstFlattened * ConstFlattened -> ConstFlattened
   max(c1, c2) = if gt(c1,c2) then c1 else c2 endif

 // CONST FLATTENED ARITHMETIC

   neg :: ConstFlattened -> ConstFlattened
   neg(c) = case c of
      top => bot;
      bot => top;
      c => lift(-drop(c));
      endcase

   add :: ConstFlattened * ConstFlattened -> ConstFlattened
   add(c1,c2) = case (c1,c2) of
      (bot,top) => error("Runtime Error: adding -inf with +inf");
      (top,bot) => error("Runtime Error: adding +inf with -inf");
      (bot,_) => bot;
      (top,_) => top;
      (_,bot) => bot;
      (_,top) => top;
      (c1,c2) => lift(drop(c1) + drop(c2));
      endcase

   sub :: ConstFlattened * ConstFlattened -> ConstFlattened
   sub(c1,c2) = add(c1,neg(c2))

   mult :: ConstFlattened * ConstFlattened -> ConstFlattened
   mult(c1,c2) =
      if eq(c2,lift(0)) then lift(0)
      else
         case (c1,c2) of
         (top,bot) => bot;
         (bot,top) => bot;
         (top,top) => top;
         (top,top) => top;
         (bot,_) => if drop(c2) > 0 then bot else top endif;
         (top,_) => if drop(c2) > 0 then top else bot endif;
         (_,bot) => if drop(c1) > 0 then bot else top endif;
         (_,top) => if drop(c1) > 0 then top else bot endif;
         (_,_) => lift(drop(c1)*drop(c2));
         endcase
      endif

   div :: ConstFlattened * ConstFlattened -> ConstFlattened
   div(c1,c2) =
      if eq(c2,lift(0))
      then error("Runtime Error: Division by zero")
      else
         case(c1,c2) of
         (top,bot) => bot;
         (bot,top) => bot;
         (top,top) => top;
         (top,top) => top;
         (bot,_) => if drop(c2) > 0 then bot else top endif;
         (top,_) => if drop(c2) > 0 then top else bot endif;
         (_,bot) => lift(0);
         (_,top) => lift(0);
         (_,_) => lift(drop(c1)/drop(c2));
         endcase
      endif

 // INTERVAL ARITHMETIC

   negItv :: Interval -> Interval
   negItv(i) = case (i#1,i#2) of
      (top,bot) => (top,bot);
      (top,top) => (top,top);
      (_,_) => (neg(i#2),neg(i#1));
      endcase

   addItv :: Interval * Interval -> Interval
   addItv(i1, i2) = case (i1,i2) of
      ((top,bot),_) => (top,bot);
      (_,(top,bot)) => (top,bot);
      ((top,top),_) => (top,top);
      (_,(top,top)) => (top,top);
      (_,_) => (add(i1#1,i2#1), add(i1#2,i2#2));
      endcase

   subItv :: Interval * Interval -> Interval
   subItv(i1, i2) = addItv(i1, negItv(i2))

   multItv :: Interval * Interval -> Interval
   multItv(i1, i2) = case (i1,i2) of
      ((top,bot),_) => (top,bot);
      (_,(top,bot)) => (top,bot);
      ((top,top),_) => (top,top);
      (_,(top,top)) => (top,top);
      (_,_) => let m1 = mult(i1#1,i2#1),
                   m2 = mult(i1#1,i2#2),
                   m3 = mult(i1#2,i2#1),
                   m4 = mult(i1#2,i2#2) in
               let minBound = min(min(m1,m2),min(m3,m4)),
                   maxBound = max(max(m1,m2),max(m3,m4)) in
               (minBound, maxBound);
      endcase

   divItv :: Interval * Interval -> Interval
   divItv(i1,i2) = case(i1,i2) of
      ((top,bot),_) => (top,bot);
      (_,(top,bot)) => (top,bot);
      ((top,top),_) => (top,top);
      (_,(top,top)) => (top,top);
      (_,_) => if ltEq(i2#1,lift(0)) && gtEq(i2#2,lift(0)) // interval contains zero
               then (bot,top)
               else
                  let m1 = div(i1#1,i2#1),
                      m2 = div(i1#1,i2#2),
                      m3 = div(i1#2,i2#1),
                      m4 = div(i1#2,i2#2) in
                  let minBound = min(min(m1,m2),min(m3,m4)),
                      maxBound = max(max(m1,m2),max(m3,m4)) in
                  (minBound, maxBound)
               endif;
      endcase

 // SET OPERATIONS ON INTERVALS

   unionItv :: Interval * Interval -> Interval
   unionItv(i1,i2) = (min(i1#1,i2#1),max(i1#2,i2#2))

   compLeftItv :: Interval -> Interval
   compLeftItv(i) = case (i#1,i#2) of
      (top,bot) => error("Runtime Error: left complement of empty interval");
      (top,top) => error("Runtime Error: left complement of unkown interval");
      (bot,_) => (top,bot);
      (_,_) => ( bot, sub(i#1, lift(1)) );
      endcase

   compRightItv :: Interval -> Interval
   compRightItv(i) = negItv(compLeftItv(negItv(i)))

   compItv :: Interval -> Interval
   compItv(i) = case i of
      (top,top) => error("Runtime Error: complement of unkown interval");
      (top,bot) => (bot,top);
      _ => unionItv(compLeftItv(i),compRightItv(i));
      endcase

   interItv :: Interval * Interval -> Interval
   interItv(i1,i2) = case (i1,i2) of
      (top,top) => error("Runtime Error: inter of unkown interval");
      ((top,bot),_) => (top,bot);
      (_,(top,bot)) => (top,bot);
      (_,_) => if lt(i1#2, i2#1) || lt(i2#2,i1#1) then (top,bot)
               else (max(i1#1,i2#1),min(i1#2,i2#2)) endif;
      endcase

   eqItv :: Interval * Interval -> Interval
   eqItv(i1,i2) = case (i1,i2) of
      ((top,top),_) => eqItv((bot,top),i2);
      (_,(top,top)) => i1;
      (_,_) => interItv(i1,i2);
      endcase

   diffItv :: Interval * Interval -> Interval
   diffItv(i1,i2) = compItv(eqItv(i1,i2))

   infItv :: Interval * Interval -> Interval
   infItv(i1,i2) = case (i1,i2) of
      ((top,top),_) => infItv((bot,top),i2);
      (_,(top,top)) => i1;
      ((top,bot),_) => (top,bot);
      (_,(top,bot)) => (top,bot);
      (_,_) => let i2WithoutMax = if i2#1=i2#2 then (top,bot) 
                                  else (i2#1,sub(i2#2,lift(1)))
                                  endif in
               let i2Inf = unionItv(compLeftItv(i2),i2WithoutMax) in 
               interItv(i1,i2Inf);
      endcase

   infEqItv :: Interval * Interval -> Interval
   infEqItv(i1,i2) = case (i1,i2) of
      ((top,top),_) => infEqItv((bot,top),i2);
      (_,(top,top)) => i1;
      ((top,bot),_) => (top,bot);
      (_,(top,bot)) => (top,bot);
      (_,_) => let i2Inf = unionItv(compLeftItv(i2),i2) in 
               interItv(i1,i2Inf);
      endcase

   supItv :: Interval * Interval -> Interval
   supItv(i1,i2) = case (i1,i2) of
      ((top,top),_) => supItv((bot,top),i2);
      (_,(top,top)) => i1;
      ((top,bot),_) => (top,bot);
      (_,(top,bot)) => (top,bot);
      (_,_) => let i2WithoutMin = if i2#1=i2#2 then (top,bot) 
                                  else (add(i2#1,lift(1)),i2#2) 
                                  endif in
               let i2Sup = unionItv(i2WithoutMin,compRightItv(i2)) in 
               interItv(i1,i2Sup);
      endcase

   supEqItv :: Interval * Interval -> Interval
   supEqItv(i1,i2) = case (i1,i2) of
      ((top,top),_) => supEqItv((bot,top),i2);
      (_,(top,top)) => i1;
      ((top,bot),_) => (top,bot);
      (_,(top,bot)) => (top,bot);
      (_,_) => let i2Sup = unionItv(i2,compRightItv(i2)) in 
               interItv(i1,i2Sup);
      endcase

 // BOOLEAN SET OPERATIONS ON INTERVALS

   eqItvB :: Interval * Interval -> BoolFlattened
   eqItvB(i1,i2) = 
      case (i1,i2) of
      ((top,top),_) => top;
      (_,(top,top)) => top;
      ((top,bot),_) => top;
      (_,(top,bot)) => top;
      (_,_) => if lt(i1#2, i2#1) || lt(i2#2,i1#1) // no intersection 
                  then lift(false)
               else if i1#1=i2#1 && i1#2=i2#2 // no difference
                  then lift(true)
               else top // intersection
               endif endif;
      endcase
   
   diffItvB :: Interval * Interval -> BoolFlattened
   diffItvB(i1,i2) = 
      let b = eqItvB(i1,i2) in
      if b = top then top 
      else lift(!drop(b))
      endif
   
   infItvB   :: Interval * Interval -> BoolFlattened
   infItvB(i1,i2) = case (i1,i2) of
     ((top,top),_) => top;
     (_,(top,top)) => top;
     ((top,bot),_) => top;
     (_,(top,bot)) => top;
     (_,_) => 
       if lt(i1#2,i2#1) then lift(true) // always <
       else if gtEq(i1#1,i2#2) then lift(false) // always >= 
       else top
       endif endif;
     endcase
   
   infEqItvB :: Interval * Interval -> BoolFlattened
   infEqItvB(i1,i2) = case (i1,i2) of
     ((top,top),_) => top;
     (_,(top,top)) => top;
     ((top,bot),_) => top;
     (_,(top,bot)) => top;
     (_,_) => 
       if ltEq(i1#2,i2#1) then lift(true) // always <=
       else if gt(i1#1,i2#2) then lift(false) // always > 
       else top
       endif endif;
     endcase
   
   supItvB :: Interval * Interval -> BoolFlattened
   supItvB(i1,i2) = infItvB(i2,i1)
   
   supEqItvB :: Interval * Interval -> BoolFlattened
   supEqItvB(i1,i2) = infEqItvB(i2,i1)
   